package co.com.claro.logistica.inventario.cargainventario;

import io.nats.streaming.*;
import org.apache.flume.*;
import org.apache.flume.conf.Configurable;
import org.apache.flume.sink.AbstractSink;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;

public class NatsSink extends AbstractSink implements Configurable {
    private String poolingInterval;
    private StreamingConnectionFactory cf;
    private StreamingConnection sc;

    @Override
    public void configure(Context context) {
        String myProp = context.getString("poolingInterval", "5");

        // Process the myProp value (e.g. validation)

        // Store myProp for later retrieval by process() method
        this.poolingInterval = myProp;
    }

    @Override
    public void start() {
        // Initialize the connection to the external repository (e.g. HDFS) that
        // this Sink will forward Events to ..
        // Create a connection factory
        cf = new StreamingConnectionFactory("test-cluster", "bar");
        // A StreamingConnection is a logical connection to the NATS streaming
        // server.  This API creates an underlying core NATS connection for
        // convenience and simplicity.  In most cases one would create a secure
        // core NATS connection and pass it in via
        // StreamingConnectionFactory.setNatsConnection(Connection nc)
        try {
            sc = cf.createConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop () {
        // Disconnect from the external respository and do any
        // additional cleanup (e.g. releasing resources or nulling-out
        // field values) ..
        try {
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Status process() throws EventDeliveryException {
        Status status = null;

        // Start transaction
        Channel ch = getChannel();
        Transaction txn = ch.getTransaction();
        System.out.println("+++++++++++++++++++++++ START SINK +++++++++++++++++++++++++++++++++");
        txn.begin();
        System.out.println("********************************************************");
        try {
            // This try clause includes whatever Channel operations you want to do

            int processedEvents = 0;
            int batchSize = 10;
            for (; processedEvents < batchSize; processedEvents += 1) {

                Event event = ch.take();

                if (event == null) {
                    // no events available in channel
                    if (processedEvents == 0) {
                        status = Status.BACKOFF;
                    } else {
                        status = Status.READY;
                    }
                    break;
                }

                // Send the Event to the external repository.
                System.out.println("**********************STORE EVENT*********************************");
                storeData(event);
                System.out.println("**********************STORE DONE*********************************");
            }

            txn.commit();
            status = Status.READY;
            System.out.println("**********************TX COMMIT*********************************");
        } catch (Throwable t) {

            System.out.println("Error : "+t.getMessage());
            t.printStackTrace();

            txn.rollback();
            // Log exception, handle individual exceptions as needed

            status = Status.BACKOFF;

            // re-throw all Errors
            if (t instanceof Error) {
                throw (Error)t;
            }
        } finally {
            if(txn != null){
                txn.close();
            }
        }
        return status;
    }

    private void storeData(Event e) throws InterruptedException, TimeoutException, IOException {




        // This simple synchronous publish API blocks until an acknowledgement
        // is returned from the server.  If no exception is thrown, the message
        // has been stored in NATS streaming.
        sc.publish("foo", "Hello World".getBytes());

        // Use a countdown latch to wait for our subscriber to receive the
        // message we published above.
//        final CountDownLatch doneSignal = new CountDownLatch(1);

        // Simple Async Subscriber that retrieves all available messages.
//        Subscription sub = sc.subscribe("foo", new MessageHandler() {
//            public void onMessage(Message m) {
//                System.out.printf("Received a message: %s\n", new String(m.getData()));
//                doneSignal.countDown();
//            }
//        }, new SubscriptionOptions.Builder().deliverAllAvailable().build());

//        doneSignal.await();

        // Unsubscribe to clean up
//        sub.unsubscribe();

        // Close the logical connection to NATS streaming
        //sc.close();
    }

    public void setPoolingInterval(String poolingInterval) {
        this.poolingInterval = poolingInterval;
    }
}
