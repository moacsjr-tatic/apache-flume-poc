package co.com.claro.logistica.inventario.cargainventario;

public class InventarioDTO {

    private int id;
    private String sku;
    private String modelo;
    private String fabricante;
    private int cantidad;

    public InventarioDTO(int id, String sku, String modelo, String fabricante, int cantidad) {
        this.id = id;
        this.sku = sku;
        this.modelo = modelo;
        this.fabricante = fabricante;
        this.cantidad = cantidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Inventario{" +
                "id=" + id +
                ", sku='" + sku + '\'' +
                ", modelo='" + modelo + '\'' +
                ", fabricante='" + fabricante + '\'' +
                ", cantidad=" + cantidad +
                '}';
    }
}
