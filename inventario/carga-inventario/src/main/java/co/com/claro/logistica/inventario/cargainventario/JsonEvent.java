package co.com.claro.logistica.inventario.cargainventario;

import com.google.gson.Gson;
import org.apache.flume.Event;

import java.util.HashMap;
import java.util.Map;

public class JsonEvent<E> implements Event {
    private Map<String, String> h = new HashMap<>();
    private E data;
    private byte[] body;

    JsonEvent(E data){
        this.data = data;
        final Gson gson = new Gson();
        this.setBody(gson.toJson(this.data).getBytes());
    }

    @Override
    public Map<String, String> getHeaders() {
        return h;
    }

    @Override
    public void setHeaders(Map<String, String> map) {
        throw new UnsupportedOperationException();
    }

    @Override
    public byte[] getBody() {
        return body;
    }

    @Override
    public void setBody(byte[] bytes) {
        this.body = bytes;
    }
}
