package co.com.claro.logistica.inventario.cargainventario;

import io.nats.streaming.*;
import org.apache.flume.Context;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.PollableSource;
import org.apache.flume.conf.Configurable;
import org.apache.flume.lifecycle.LifecycleState;
import org.apache.flume.source.AbstractSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class NatsSource extends AbstractSource implements Configurable, PollableSource {

    private AnnotationConfigApplicationContext appContext;
    private StreamingConnectionFactory cf;
    private StreamingConnection sc;

    @Override
    public LifecycleState getLifecycleState() {
        return null;
    }

    @Override
    public Status process() throws EventDeliveryException {
        Status status = null;

        try {
            // This try clause includes whatever Channel/Event operations you want to do

            // Receive new data
            List<InventarioDTO> inventarioList = getData();

            System.out.println("Obtuve "+inventarioList.size()+" registros");
            for(InventarioDTO inventario : inventarioList){
                // Store the Event into this Source's associated Channel(s)
                System.out.println("Enviando "+inventario.toString());
                getChannelProcessor().processEvent(new JsonEvent<InventarioDTO>(inventario));
                System.out.println("Done");
            }
            status = Status.READY;
        } catch (Throwable t) {
            // Log exception, handle individual exceptions as needed

            status = Status.BACKOFF;

            // re-throw all Errors
            if (t instanceof Error) {
                throw (Error)t;
            }
        } finally {


        }
        return status;
    }

    private List<InventarioDTO> getData() throws Exception{
        //Obtener datos de Nats
        Subscription sub = sc.subscribe("foo", new MessageHandler() {
            public void onMessage(Message m) {
                System.out.printf("Received a message: %s\n", new String(m.getData()));
            }
        }, new SubscriptionOptions.Builder().deliverAllAvailable().build());


        // Unsubscribe to clean up
        sub.unsubscribe();
        return null;
    }

    @Override
    public long getBackOffSleepIncrement() {
        return 0;
    }

    @Override
    public long getMaxBackOffSleepInterval() {
        return 0;
    }

    @Override
    public void configure(Context context) {

        appContext = new AnnotationConfigApplicationContext(CargaInventarioApplication.class);
        // Initialize the connection to the external repository (e.g. HDFS) that
        // this Sink will forward Events to ..
        // Create a connection factory
        cf = new StreamingConnectionFactory("test-cluster", "bar");
        // A StreamingConnection is a logical connection to the NATS streaming
        // server.  This API creates an underlying core NATS connection for
        // convenience and simplicity.  In most cases one would create a secure
        // core NATS connection and pass it in via
        // StreamingConnectionFactory.setNatsConnection(Connection nc)
        try {
            sc = cf.createConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
