package co.com.claro.logistica.inventario.cargainventario;

import com.sun.rowset.internal.Row;
import org.apache.flume.*;
import org.apache.flume.conf.Configurable;
import org.apache.flume.lifecycle.LifecycleState;
import org.apache.flume.source.AbstractSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import sun.awt.X11.XSystemTrayPeer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

class InventarioRowMapper implements RowMapper<InventarioDTO>{

    @Override
    public InventarioDTO mapRow(ResultSet rs, int i) throws SQLException {
        return new InventarioDTO(rs.getInt("sku"),
                rs.getString("name"),
                rs.getString("modelo"),
                rs.getString("fabricante"),
                rs.getInt("cantidad"));
    }
}


public class OracleJDBCSource extends AbstractSource implements Configurable, PollableSource {

    private JdbcTemplate jdbc;
    private AnnotationConfigApplicationContext cf;
    private InventarioRowMapper rowMapper = new InventarioRowMapper();

    @Override
    public LifecycleState getLifecycleState() {
        return null;
    }

    @Override
    public Status process() throws EventDeliveryException {
        Status status = null;

        try {
            // This try clause includes whatever Channel/Event operations you want to do

            // Receive new data
            List<InventarioDTO> inventarioList = getData();

            System.out.println("Obtuve "+inventarioList.size()+" registros");
            for(InventarioDTO inventario : inventarioList){
                // Store the Event into this Source's associated Channel(s)
                System.out.println("Enviando "+inventario.toString());
                getChannelProcessor().processEvent(new JsonEvent<InventarioDTO>(inventario));
                System.out.println("Done");
            }
            status = Status.READY;
        } catch (Throwable t) {
            // Log exception, handle individual exceptions as needed

            status = Status.BACKOFF;

            // re-throw all Errors
            if (t instanceof Error) {
                throw (Error)t;
            }
        } finally {


        }
        return status;
    }

    private List<InventarioDTO> getData() {
        List<InventarioDTO> inventarioList = this.jdbc.query("select * from stock", rowMapper);
        //System.out.println(inventarioList.toString());
        return inventarioList;
    }

    @Override
    public long getBackOffSleepIncrement() {
        return 0;
    }

    @Override
    public long getMaxBackOffSleepInterval() {
        return 0;
    }

    @Override
    public void configure(Context context) {

        cf = new AnnotationConfigApplicationContext(CargaInventarioApplication.class);
        jdbc = (JdbcTemplate) cf.getBean("jdbcTemplate");

    }
}
